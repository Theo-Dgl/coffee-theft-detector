import requests
import picamera
from time import sleep
import datetime
import logging

logging.basicConfig(format='%(asctime)s %(message)s', filename='app.log',level=logging.INFO)
api_url = 'http://192.168.1.1'

def check_alert():
    try:
        response = requests.get(api_url)
        if response.status_code == 200:
                record_theft()
                logging.warning("alert detected")
    except Exception as ex:
                logging.error("no connection avaible")
                print(ex)
                pass

def record_theft():
        video_name = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
        video_name += ".h264"
        with picamera.PiCamera() as camera:
                camera.resolution = (640, 480)
                camera.start_recording(video_name)
                camera.wait_recording(60)
                camera.stop_recording()
                logging.info("recording has stop")

logging.info("app started")
while True:
        check_alert()
        sleep(1)