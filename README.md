# Coffee-theft-detector

Start a video camera recording when someone stole my coffee

## Hardware

- RPI Zero W + Camera PI
- ESP8266 (embded with a tilt sensor)
- An external phone battery (to power up the ESP)

 ![Wiring plan](asset/scheme_printed_circuit.png)

## How it works

The ESP 8266 acts as a wifi access point and also hosts a web server.

When moving the ESP8266, the ESP returns an HTTP code 200 instead of an HTTP code 204.

The raspberry makes calls every second to the ESP web server, and starts video recording for one minute.

The videos can then be retrieved by any means you choose from the Raspberry.

## Way of improvment

In order to increase the autonomy of the ESP, it would be preferable to host the Wifi access point on the RPI.

In addition, using UDP communication would also be less energy consuming.