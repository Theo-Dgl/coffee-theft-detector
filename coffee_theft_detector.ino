#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

#define HTTP_SERVER_PORT 80

const int sensorPin = 2;//the led attach to

//web server part
const char* wifi_ssid = "COFFEE";
const char* wifi_passwd = "MyWifiPassword";

IPAddress local_IP(192,168,1,1);
IPAddress gateway(192,168,1,254);
IPAddress subnet(255,255,255,0);

ESP8266WebServer web_server(local_IP, HTTP_SERVER_PORT);

int previousValue;
bool alert;

void setup()
{
	initAP();
	config_server_routing();

	pinMode(LED_BUILTIN,OUTPUT);//builtin led
	pinMode(sensorPin,INPUT);
}

void loop() 
{  
	int digitalVal = digitalRead(sensorPin);

	if(digitalVal != previousValue) {
		previousValue = digitalVal;
		alert = true;
		Serial.println("alert");
	}

	web_server.handleClient();
}

void initAP() 
{
	Serial.begin(115200);
	while(!Serial){}
	Serial.println("connection establised");
	
	Serial.print("setting soft AP config... -> ");
	Serial.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "Ready" : "Failed");
	
	Serial.print("setting soft AP... -> ");
	Serial.println(WiFi.softAP(wifi_ssid,wifi_passwd,1,false,2) ? "Ready" : "Failed");  

	Serial.print("AP ip adress -> ");
	Serial.println(WiFi.softAPIP());
}

void handleRoot() {
	if(alert) {
		web_server.send(200, "text/plain","ARMAGEDDON");
		alert = false;
	} else {
		web_server.send(204, "text/plain","");
	}
}

void handleNotFound() {
	web_server.send(404, "text/plain", "There is no such file...");
}

void config_server_routing()
{
	Serial.println("config_server_routing");
	web_server.on("/", handleRoot);
	web_server.onNotFound(handleNotFound);
	web_server.begin();
	Serial.println("web server started");
}
